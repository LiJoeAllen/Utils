package top.joeallen.log;

/**
 * 工具使用
 *
 * @author JoeAllen
 */
public class Log {
    public static void i(Object o) {
        Out.print(OutputType.INFO, o);
    }

    public static void d(Object o) {
        Out.print(OutputType.DEBUG, o);
    }

    public static void w(Object o) {
        Out.print(OutputType.WARNING, o);
    }

    public static void json(Object o) {
        Out.print(OutputType.JSON, o);
    }
}
