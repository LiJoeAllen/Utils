package top.joeallen.log;

/**
 * 数据常量
 *
 * @author JoeAllen
 */

public class Constant {
    /**
     * 颜色
     */
    protected static final int[] COLOR = {29, 30, 31, 32, 33, 34, 35, 36, 37};
    /**
     * 字体样式
     * 1    加粗
     * 2    默认
     * 3    斜体
     * 4    下划线
     */
    protected static final int[] FONT_STYLE_CODE = {1, 2, 3, 4};

}
