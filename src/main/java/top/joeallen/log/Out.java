package top.joeallen.log;

import top.joeallen.Utils;

/**
 * 底层实现
 *
 * @author JoeAllen_Li
 */
public class Out {
    /**
     * 执行所有输出操作
     *
     * @param o 输出内容
     */
    public static void print(OutputType outputType, Object o) {
        printSingleColor(Utils.getDate(), outputType.getColor(), Configuration.getInstance().getFontStyle(), trace(outputType), dataJudge(o));
    }

    /**
     * 数据判断
     *
     * @param o 数据
     * @return 数据类型
     */
    private static String dataJudge(Object o) {
        String type=Utils.getType(o);
//        if(){
//
//        }
        return Utils.getType(o) + "\t" + o;
    }

    /**
     * 定位代码位置
     *
     * @param outputType 输出类型
     * @return 返回所在包类行
     */
    public static String trace(OutputType outputType) {
        Throwable throwable = new Throwable();
        StackTraceElement ste;
        ste = throwable.getStackTrace()[Configuration.getInstance().getSteNum()];
        return outputType.getName() + "\t" + ste;
    }

    /**
     * @param pattern 前面的图案 such as "=============="
     * @param code    颜色代号：背景颜色代号(41-46)；前景色代号(31-36)
     * @param n       数字+m  1——加粗  3——斜体  4——下划线
     * @param content 前缀
     * @param text    要打印的内容
     */
    public static void printSingleColor(String pattern, int code, int n, String content, String text) {
        System.out.format("%s\t\33[%d;%dm%s\t%s%n", pattern, code, n, content, text).format("\33[%dm", 0);
    }
}