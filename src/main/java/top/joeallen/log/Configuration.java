package top.joeallen.log;

/**
 * 功能配置
 *
 * @author JoeAllen
 */
public class Configuration {
    //region 单例

    private static class SingletonHolder {
        private static final Configuration INSTANCE = new Configuration();
    }

    private Configuration() {
    }

    public static Configuration getInstance() {
        return SingletonHolder.INSTANCE;
    }
    // endregion

    //region 代码定位层数

    private int steNum = 3;

    public int getSteNum() {
        return steNum;
    }

    public Configuration setSteNum(int steNum) {
        this.steNum = steNum;
        return this;
    }
    // endregion

    //region 字体样式

    private int fontStyle = FontStyle.FONT_STYLE_DEFAULT;

    public int getFontStyle() {
        return fontStyle;
    }

    public Configuration setFontStyle(int fontStyle) {
        this.fontStyle = fontStyle;
        return this;
    }
    // endregion

    public static class ColorConfiguration {

        public static Configuration su() {
            return Configuration.getInstance();
        }
    }

    public void init() {

    }
}
