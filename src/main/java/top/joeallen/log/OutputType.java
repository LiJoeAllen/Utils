package top.joeallen.log;

/**
 * Assert
 * Debug
 * Error
 * Info
 * Verbose
 * Warning
 *
 * @author JoeAllen
 */
public enum OutputType {
    /**
     * INFO
     */
    INFO("INFO", Color.white, 0),
    DEBUG("DEBUG", Color.yellow, 1),
    WARNING("WARNING", Color.red, 2),
    JSON("JSON", Color.white, 3);

    OutputType(String name, int color, int index) {
        this.name = name;
        this.color = color;
        this.index = index;
    }

    private final String name;
    private int color;
    private final int index;

    public String getName() {
        return name;
    }


    public int getColor() {
        return color;
    }

    public OutputType setColor(int color) {
        this.color = color;
        return this;
    }

    public int getIndex() {
        return index;
    }


    @Override
    public String toString() {
        return "OutputType{" +
                "name='" + name + '\'' +
                ", index=" + index +
                '}';
    }
}
