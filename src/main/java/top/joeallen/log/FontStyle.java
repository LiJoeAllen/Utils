package top.joeallen.log;

/**
 * @author JoeAllen
 */
public final class FontStyle {
    public static final int FONT_STYLE_BOLD = Constant.FONT_STYLE_CODE[0];
    public static final int FONT_STYLE_DEFAULT = Constant.FONT_STYLE_CODE[1];
    public static final int FONT_STYLE_ITALICS = Constant.FONT_STYLE_CODE[2];
    public static final int FONT_STYLE_UNDERLINE = Constant.FONT_STYLE_CODE[3];
}