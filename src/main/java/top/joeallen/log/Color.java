package top.joeallen.log;

public class Color {
    public static int white = Constant.COLOR[0];
    public static int black = Constant.COLOR[1];
    public static int red = Constant.COLOR[2];
    public static int green = Constant.COLOR[3];
    public static int yellow = Constant.COLOR[4];
    public static int cyan = Constant.COLOR[5];
    public static int purple = Constant.COLOR[6];
    public static int aquamarine = Constant.COLOR[7];
    public static int gray = Constant.COLOR[7];
}
