package top.joeallen;

import java.lang.reflect.Array;
import java.lang.reflect.Field;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * 工具
 *
 * @author JoeAllen
 */
public class Utils {
    /**
     * 获取时间数据
     *
     * @return 当前时间（精确到毫秒）
     */
    public static String getDate() {
        SimpleDateFormat sdf = new SimpleDateFormat();
        sdf.applyPattern("yyyy-MM-dd HH:mm:ss:SSS");
        return sdf.format(new Date());
    }

    /**
     * 获取数据类型
     *
     * @param a 目标数据
     * @return 数据类型名称
     */
    public static String getType(Object a) {
        return a.getClass().getSimpleName();
    }

    private static int getLength(Object obj) {
        if (obj == null) {
            return 0;
        }
        if (obj.getClass().isArray()) {
            return Array.getLength(obj);
        }
        if (obj instanceof List) {
            List<Object> result = new ArrayList<>((List<?>) obj);
            return (result.size());
        }
        if (obj instanceof Set) {
            Set<Object> result = new HashSet<>((Set<?>) obj);
            return (result.size());
        }
        if (obj instanceof Map) {
            return (getObjectToMap(obj).size());
        }
        return String.valueOf(obj).length();

    }

    /**
     * Object转Map
     *
     * @param obj Object
     * @return map
     */
    public static Map<String, Object> getObjectToMap(Object obj) {
        Map<String, Object> map = new LinkedHashMap<>();
        Class<?> clazz = obj.getClass();
        for (Field field : clazz.getDeclaredFields()) {
            field.setAccessible(true);
            String fieldName = field.getName();
            Object value;
            try {
                value = field.get(obj);
            } catch (IllegalAccessException e) {
                return map;
            }
            if (value == null) {
                value = "";
            }
            map.put(fieldName, value);
        }
        return map;
    }
}
