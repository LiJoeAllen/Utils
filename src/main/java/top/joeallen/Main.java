package top.joeallen;

import java.util.ArrayList;
import java.util.List;

import top.joeallen.log.Configuration;
import top.joeallen.log.FontStyle;
import top.joeallen.log.Log;

/**
 * @author JoeAllen
 */
public class Main {

    public static void main(String[] args) {
        Configuration
                .getInstance()
                .setSteNum(3)
                .setFontStyle(FontStyle.FONT_STYLE_DEFAULT)
                .init();

        Log.i("Log");
        Log.d("Log");
        Log.w("Log");
        Log.json("Log");

    }
}
