[![](https://jitpack.io/v/LiJoeAllen/Utils.svg)](https://jitpack.io/#LiJoeAllen/Utils)[![License](https://img.shields.io/badge/License-Apache%202.0-blue.svg)](http://www.apache.org/licenses/LICENSE-2.0)

## 简介

**Utils**帮助您在各种Java项目中轻松使用**便捷工具**
项目使用Java8和Gradle构造

## 用法

### 引入库

你可以选择下面的任意一种方式引入本库

- Gradle
在项目级别的`build.gradle`中添加Maven仓库 ` maven { url "https://jitpack.io"}` 并在模块级别的 `build.gradle` 中添加依赖`implementation 'com.github.LiJoeAllen:Utils:1.0.0`

- Maven
在maven的conf/settings.xml中配置阿里仓库并把mirrorOf节点中添加`,!jitpack.io`
`<mirrorOf>*</mirrorOf>`改成`<mirrorOf>*,!jitpack.io</mirrorOf>`
```xml
<mirror>
    <id>aliyunmaven</id>
    <mirrorOf>*,!jitpack.io</mirrorOf>
    <name>阿里云公共仓库</name>
    <url>https://maven.aliyun.com/repository/public</url>
</mirror>
```
在项目pom.xml文件的project节点中添加
```xml
 <repositories>
     <repository>
         <id>jitpack.io</id>
         <url>https://jitpack.io</url>
     </repository>
</repositories>
```
在dependencies节点中引入包
```xml
<dependency>
    <groupId>com.github.LiJoeAllen</groupId>
    <artifactId>Utils</artifactId>
    <version>1.0.0</version>
</dependency>
```

### 在代码中使用（暂时只提供日志输出功能）

```Java
Log.i("Log");
Log.d("Log");
Log.w("Log");
Log.json("Log");
```
对，就是这么简单！

### 其他
输入工具的使用简单功能强大，最大特点是可以定位代码位置
在输出信息中依次包含时间信息、输出类型、输出代码所在项目源码中的位置、输出数据的数据类型及数据本身

输出工具借鉴了Java源码中抛出异常的代码定位功能及 Android Logcat的实现效果

![rendering1.png](rendering1.png)
![rendering2.png](rendering2.png)
